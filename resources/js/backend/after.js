$('.btn-import-csv-file').on('click', function () {
    let csv_type = $(this).attr('data-csv-type');
    let modal_element = $('#import-csv-file-modal');
    $('#import-csv-file-modal input[name=csv_type]').val(csv_type);
    $('#import-csv-file-modal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
});

$('.btn-analytics-crawl').on('click', function () {
    $('#craw-data-ga').modal('show');
});

$('.btn-update-adword-data').on('click', function () {
    $('#craw-data-adword').modal('show');
});

$('#import-csv-file-modal form button[type=submit]').on('click', function (e) {
    e.preventDefault();
    let formData = new FormData($('#import-csv-file-modal form')[0]);
    let action = $('#import-csv-file-modal form').attr('action');
    $.ajax({
        url: action,
        type: 'POST',
        data: formData,
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                }
            }, false);
            return xhr;
        },
        success: function(data) {
            alert('success!');
            $('#import-csv-file-modal form input[type=file]').val('');
            $('#import-csv-file-modal').modal('hide');
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;
});

$('.js-change-income').on('change', function () {
    let income = $(this).val();
    let url = $(this).attr('data-url');
    console.log(income);
    $.ajax({
        url: url,
        type: 'PUT',
        data: {
            income: income
        },
        success: function(response) {
            if (response.status === true) {
                alert('success!');
            } else {
                alert(response.msg);
            }
        }
    });
});

$('.js-btn-delete').on('click', function () {
    let url = $(this).attr('href');
    $('#delele-item-modal form').attr('action', url);
    $('#delele-item-modal').modal('show');
    return false;
});
