import $ from 'jquery';
import 'popper.js/dist/umd/popper'; // Required for BS4
import 'bootstrap';

require('webpack-jquery-ui');
require('webpack-jquery-ui/css');

import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';

library.add(fab, far, fas);

// Kicks off the process of finding <i> tags and replacing with <svg>
dom.watch();

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = $;
