<footer class="sticky-footer">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright © Ads Analytics {{ date('Y') }}</span>
        </div>
    </div>
</footer>
