<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item {{ active_class(Active::checkUriPattern('/'), 'active') }}">
        <a class="nav-link" href="{{ route('web.dashboard') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span> ダッシュボード </span>
        </a>
    </li>
    <li class="nav-item {{ active_class(Active::checkUriPattern(['campaign', 'campaign/*']), 'active') }}">
        <a class="nav-link" href="{{ route('web.campaign') }}">
        <i class="far fa-list-alt"></i>
        <span> キャンペーン </span></a>
    </li>

    <li class="nav-item {{ active_class(Active::checkUriPattern(['asp', 'asp/*']), 'active') }}">
        <a class="nav-link" href="{{ route('web.asp.index') }}">
        <i class="fas fa-fw fa-table"></i>
        <span> CV </span></a>
    </li>

    <li class="nav-item {{ active_class(Active::checkUriPattern(['adword-account', 'adword-account/*']), 'active') }}">
        <a class="nav-link" href="{{ route('web.adword-account.index') }}">
        <i class="fas fa-fw fa-user"></i>
        <span> アドワーズアカウント </span></a>
    </li>

    <li class="nav-item {{ active_class(Active::checkUriPattern(['user', 'user/*']), 'active') }}">
        <a class="nav-link" href="{{ route('web.user.index') }}">
        <i class="fas fa-fw fa-users"></i>
        <span> ユーザ </span></a>
    </li>
</ul>
