@if (session('alert'))
    <div class="alert alert-success">
         @foreach (session('alert') as $alert)
            <li>{{ $alert }}</li>
        @endforeach
    </div>
@endif
