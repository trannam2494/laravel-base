@if ($errors->any())
    <div class="alert alert-danger alert-errors-custom">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
