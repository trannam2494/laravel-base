@extends('layouts.app')

@section('title', 'User')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <a href="{{ route('web.user.create') }}" title="Add new" class="btn btn-primary">Add new</a>
        </div>
        <div class="card-body">
            @include('includes.alerts.errors')
            @include('includes.alerts.alert')
            <div class="table-responsive">
                <table class="table table-bordered" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No .</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key => $user)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at_format }}</td>
                                <td>{!! $user->action_button !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="pagination-custom-style text-right">
            {{ $users->links() }}
        </div>
    </div>

    <div class="modal fade" id="delele-item-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="delete">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Delete User</h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                           <h3>Are you sure?</h3>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"> Submit </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
