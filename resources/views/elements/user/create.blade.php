@extends('layouts.app')

@section('title', 'Adword Account')

@section('content')
    <div class="card mb-3">
        <div class="card-header">
            <h3>Add new adword account</h3>
        </div>
        <div class="card-body">
            @include('includes.alerts.errors')
            @include('includes.alerts.alert')
            <form action="{{ route('web.user.store') }}" method="POST">
                @csrf
                <div class="form-group row">
                    <label for="staticName" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="staticName" placeholder="Account name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" name="email" class="form-control" id="staticEmail" placeholder="email@example.com">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputCustomerId" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" id="inputCustomerId" placeholder="Password">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ route('web.user.index') }}" class="btn btn-danger">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="pagination-custom-style text-right">

        </div>
    </div>
@endsection
