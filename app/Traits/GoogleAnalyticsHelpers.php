<?php

namespace App\Traits;

use Exception;
use Log;

trait GoogleAnalyticsHelpers
{
    private function initializeAnalytics()
    {

        // Use the developers console and download your service account
        // credentials in JSON format. Place them in this directory or
        // change the key file location if necessary.
        $KEY_FILE_LOCATION = config('ga.service_account_path');

        // Create and configure a new client object.
        $client = new \Google_Client();
        $client->setApplicationName(config('ga.app_name'));
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(config('ga.scope'));
        $analytics = new \Google_Service_AnalyticsReporting($client);

      return $analytics;
    }

    public function getGaReportData(string $sessionId, string $referrer_url, string $startDate = "30daysAgo", string $endDate = "yesterday")
    {
        $analytics = $this->initializeAnalytics();
        // Replace with your view ID, for example XXXX.
        $VIEW_ID = $this->getViewId($referrer_url);

        // Create the DateRange object.
        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($startDate);
        $dateRange->setEndDate($endDate);

        // Create the Metrics object.
        $sessions = new \Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:sessions");

        $bounces = new \Google_Service_AnalyticsReporting_Metric();
        $bounces->setExpression("ga:bounces");

        // Create Dimension Filter.
        $dimensionFilter = new \Google_Service_AnalyticsReporting_DimensionFilter();
        $dimensionFilter->setDimensionName("ga:dimension3");
        $dimensionFilter->setOperator("EXACT");
        $dimensionFilter->setExpressions(array($sessionId));

        // Create the DimensionFilterClauses
        $dimensionFilterClause = new \Google_Service_AnalyticsReporting_DimensionFilterClause();
        $dimensionFilterClause->setFilters(array($dimensionFilter));

        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId($VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setDimensions($this->listFieldGet());
        $request->setDimensionFilterClauses(array($dimensionFilterClause));
        $request->setMetrics(array($sessions, $bounces));

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($request));

        return $this->printResults($analytics->reports->batchGet($body));
    }

    private function listFieldGet()
    {
        $landingPagePath = new \Google_Service_AnalyticsReporting_Dimension();
        $landingPagePath->setName("ga:landingPagePath");

        $dimension3 = new \Google_Service_AnalyticsReporting_Dimension();
        $dimension3->setName("ga:dimension3");

        $keyword = new \Google_Service_AnalyticsReporting_Dimension();
        $keyword->setName("ga:keyword");

        $campaign = new \Google_Service_AnalyticsReporting_Dimension();
        $campaign->setName("ga:campaign");

        $adGroup = new \Google_Service_AnalyticsReporting_Dimension();
        $adGroup->setName("ga:adGroup");

        return array(
            $landingPagePath,
            $dimension3,
            $keyword,
            $campaign,
            $adGroup
        );
    }

    private function printResults($reports) {
        for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
            $report = $reports[ $reportIndex ];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();
            $data = [];
            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                    $row = $rows[ $rowIndex ];
                    $dimensions = $row->getDimensions();
                    $metrics = $row->getMetrics();
                    for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                        $data[] = [
                            $dimensionHeaders[$i] => $dimensions[$i]
                        ];
                    }
            }
            return $data;
        }
    }

    private function getDomainName(string $url)
    {
        $url = parse_url($url);
        $domain = isset($url['host']) ? $url['host'] : $url['path'];

        return $domain;
    }

    private function getViewId(string $url)
    {
        $domain = $this->getDomainName($url);
        $config = config('ga.view_id');

        if (!array_key_exists($domain, $config)) {

            return null;
        }

        return $config[$domain];
    }

    public function getParamsFromUrl(string $url, string $name)
    {
        try {
            preg_match("/&?" . $name . "=([^&]+)/", $url, $matches);
            return $matches[1];
        } catch (\Exception $e) {
            return null;
        }
    }
}
