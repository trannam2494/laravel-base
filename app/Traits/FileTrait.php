<?php

namespace App\Traits;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Exception;
use Log;

trait FileTrait
{
    protected $local;

    public function __construct()
    {
        $this->initLocal();
    }

    private function initLocal()
    {
        try {
            $adapter = new Local(config('filesystems.disks.upload_folder_path'));

            $this->local = new Filesystem($adapter);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function uploadFileToPath($file, $path)
    {
        try {
            // Encrypt image name
            $fileNameEncrypt = $path . '/' . encrypt_file_name(
                random_st(),
                $file->getClientOriginalExtension()
            );

            $this->local->put(
                $fileNameEncrypt,
                file_get_contents($file)
            );
            return $fileNameEncrypt;
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
        return false;
    }
}
