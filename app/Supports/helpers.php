<?php

if (! function_exists('encrypt_file_name')) {
    /**
     * Encrypt file name
     *
     * @param string $id
     * @param string $ext
     * @return string
     */
    function encrypt_file_name(string $id, string $ext)
    {
        return sprintf("%s.%s", md5($id), $ext);
    }
}

if (! function_exists('random_st')) {
    /**
    * Random unique string over time
    *
    * @return string
    */
    function random_st()
    {
        return md5(uniqid(rand(), true));
    }
}

if (! function_exists('getSidFromUrl')) {
    /**
    * Random unique string over time
    *
    * @return string
    */
    function getSidFromUrl($url)
    {
        try {
            preg_match("/&?sid=([^&]+)/", $url, $matches);
            return $matches[1];
        } catch (\Exception $e) {
            return null;
        }

    }
}
